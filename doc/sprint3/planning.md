RCOMP 2021-2022 Project - Sprint 3 planning
===========================================
### Sprint master: 1191590 (Hugo Jorge) ###

# 1. Sprint's backlog #

In this sprint the team is required to elaborate some tasks:
1. DHCPv4 service
2. VoIP service
3. Adding a second server to each DMZ network to run the HTTP service.
4. Configuring DNS servers to establish a DNS domains tree.
5. Enforcing NAT (Network Address Translation).
6. Establishing traffic access policies on routers (static firewall).

All these tasks must be accomplished using the previous sprint's packet tracker file for the campus.

Each team-member will deliver that file with all the configurations reagarding their building.

# 2. Technical decisions and coordination #

We will name our ospf 'ospf 1' and the areas will be distributed by buildings as such:
- Building 1: area 1
- Building 2: area 2
- Building 3: area 3

The prefixes for the phones will follow the same pattern:
- Building 1: 1... (1001, 1002, ...)
- Building 2: 2... (2001, 2002, ...)
- Building 3: 3... (3001, 3002, ...)

DNS domain for all the buildings:
- Building 1: rcomp-21-22-df-g2 / ns.rcomp-21-22-df-g2
- Building 2: building-2.rcomp-21-22-df-g2 / ns.building-2.rcomp-21-22-df-g2
- Building 3: building-3.rcomp-21-22-df-g2 / ns.building-3.rcomp-21-22-df-g2

# 3. Subtasks assignment #

  * 1191590 - (T.3.1) Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 1.


  * 1200585 - (T.3.2) Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 2.
Final integration of each member’s Packet Tracer simulation into a
single simulation.


  * 1191631 - (T.3.3) Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 3.

