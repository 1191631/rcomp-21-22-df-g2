RCOMP 2021-2022 Project - Sprint 3 - Member 1191631folder

## Building 3

In this document the commands used in the CLI to configure the many devices will be shown.

Please take in consideration the following table from the previous sprint:

| VLAN            | VLAN ID |   Subnet Address   |       Net Mask        |    Available Address Range        | Broadcast Address  |
|-----------------|---------|--------------------|-----------------------|-----------------------------------|--------------------|
| 3_Floor0        | 284     | 172.17.2.0         | /26 - 255.255.255.192 |    172.17.2.1 - 172.17.2.62       | 172.17.2.63        |
| 3_Floor1        | 285     | 172.17.2.64        | /26 - 255.255.255.192 |    172.17.2.65 - 172.17.2.126     | 172.17.2.127       |
| 3_Wi-Fi         | 286     | 172.17.2.128       | /26 - 255.255.255.192 |    172.17.2.129 - 172.17.2.190    | 172.17.2.191       |
| 3_DMZ           | 287     | 172.17.2.192       | /27 - 255.255.255.224 |    172.17.2.193 - 172.17.2.222    | 172.17.2.223       |
| 3_VoIP          | 288     | 172.17.2.224       | /27 - 255.255.255.224 |    172.17.2.225 - 172.17.2.254    | 172.17.2.255       |


## Notes

* IPv4 node address each router will use in the backbone network is ip address 172.16.251.5
* The sever with the address 172.17.2.194 is to support the local DNS domain.
* The sever with the address 172.17.2.195 is the HTTP service.


##Interfaces

3_Floor0
interface FastEthernet0/0.1
 encapsulation dot1Q 284
 ip address 172.17.2.1 255.255.255.192
 ip nat inside

3_Floor1
interface FastEthernet0/0.2
 encapsulation dot1Q 285
 ip address 172.17.2.65 255.255.255.192
 ip nat inside

3_Wi-Fi
interface FastEthernet0/0.3
 encapsulation dot1Q 286
 ip address 172.17.2.129 255.255.255.192
 ip nat inside

3_DMZ
interface FastEthernet0/0.4
 encapsulation dot1Q 287
 ip address 172.17.2.193 255.255.255.224
 ip nat inside

3_VoIP
interface FastEthernet0/0.5
 encapsulation dot1Q 288
 ip address 172.17.2.225 255.255.255.224
 ip nat inside

BackBone
interface FastEthernet0/0.6
 encapsulation dot1Q 275
 ip address 172.16.251.5 255.255.255.128
 ip nat outside


##OSPF

router ospf 1
 log-adjacency-changes
 network 172.16.251.0 0.0.0.127 area 0
 network 172.17.2.0 0.0.1.255 area 3


##DHCP

ip dhcp excluded-address 172.17.2.225
ip dhcp excluded-address 172.17.2.1
ip dhcp excluded-address 172.17.2.65
ip dhcp excluded-address 172.17.2.129

ip dhcp pool 3_VoIP
 network 172.17.2.224 255.255.255.224
 default-router 172.17.2.255
 option 150 ip 172.17.2.225
 dns-server 172.17.2.194
 domain-name building-3.rcomp-21-22-df-g2

ip dhcp pool 3_Floor0
 network 172.17.2.0 255.255.255.192
 default-router 172.17.2.1
 dns-server 172.17.2.194
 domain-name building-3.rcomp-21-22-df-g2

ip dhcp pool 3_Floor1
 network 172.17.2.64 255.255.255.192
 default-router 172.17.2.65
 dns-server 172.17.2.194
 domain-name building-3.rcomp-21-22-df-g2

ip dhcp pool 3_Wifi
 network 172.17.2.128 255.255.255.192
 default-router 172.17.2.129
 dns-server 172.17.2.194
 domain-name building-3.rcomp-21-22-df-g2


##NAT

ip nat inside source list 10 interface FastEthernet0/0.6 overload
ip nat inside source static tcp 172.17.2.195 80 172.16.221.5 80 
ip nat inside source static tcp 172.17.2.195 443 172.16.221.5 443 
ip nat inside source static tcp 172.17.2.194 53 172.16.221.5 53 

access-list 10 permit 171.17.2.192 0.0.0.31


##VoIP
 
telephony-service
 max-ephones 2
 max-dn 2
 ip source-address 172.17.2.255 port 2000
 auto assign 1 to 2

ephone-dn 1
 number 3001

ephone-dn 2
 number 3002

dial-peer voice 1 voip
 destination-pattern 1...
 session target ipv4:172.16.250.195

dial-peer voice 2 voip
 destination-pattern 2...
 session target ipv4:172.16.253.241