!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.16.253.193
ip dhcp excluded-address 172.16.253.129
ip dhcp excluded-address 172.16.253.1
ip dhcp excluded-address 172.16.253.225
ip dhcp excluded-address 172.16.253.241
ip dhcp excluded-address 172.16.251.4
!
ip dhcp pool 2_Floor0
 network 172.16.253.192 255.255.255.224
 default-router 172.16.253.193
 dns-server 172.16.253.225
 domain-name building-2.rcomp-21-22-df-g2
ip dhcp pool 2_Floor1
 network 172.16.253.128 255.255.255.192
 default-router 172.16.253.129
 dns-server 172.16.253.225
 domain-name building-2.rcomp-21-22-df-g2
ip dhcp pool 2_Wifi
 network 172.16.253.0 255.255.255.128
 default-router 172.16.253.1
 dns-server 172.16.253.225
 domain-name building-2.rcomp-21-22-df-g2
ip dhcp pool 2_VoIP
 network 172.16.253.240 255.255.255.240
 default-router 172.16.253.241
 option 150 ip 172.16.253.241
 dns-server 172.16.253.225
 domain-name building-2.rcomp-21-22-df-g2
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX101742GD-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 ip access-group 100 in
 duplex auto
 speed auto
!
interface FastEthernet0/0.275
 encapsulation dot1Q 275
 ip address 172.16.251.4 255.255.255.128
 ip nat outside
!
interface FastEthernet0/0.277
 encapsulation dot1Q 277
 ip address 172.16.253.193 255.255.255.224
 ip nat inside
!
interface FastEthernet0/0.278
 encapsulation dot1Q 278
 ip address 172.16.253.129 255.255.255.192
 ip nat inside
!
interface FastEthernet0/0.279
 encapsulation dot1Q 279
 ip address 172.16.253.1 255.255.255.128
 ip nat inside
!
interface FastEthernet0/0.280
 encapsulation dot1Q 280
 ip address 172.16.253.225 255.255.255.240
 ip nat inside
!
interface FastEthernet0/0.281
 encapsulation dot1Q 281
 ip address 172.16.253.241 255.255.255.240
 ip nat inside
!
interface FastEthernet0/1
 no ip address
 ip access-group FROM-LOCAL in
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 172.16.251.0 0.0.0.127 area 0
 network 172.16.252.0 0.0.1.255 area 2
!
router rip
!
ip nat inside source list 10 interface FastEthernet0/0.275 overload
ip nat inside source static tcp 172.16.253.1 80 172.16.221.5 8081 
ip nat inside source static tcp 172.16.253.1 443 172.16.221.5 8082 
ip classless
!
ip flow-export version 9
!
!
access-list 1 permit host 172.16.253.244
access-list 1 permit 0.0.0.4 255.255.255.240
access-list 2 deny 0.0.0.4 255.255.255.240
access-list 2 permit any
access-list 100 deny ip 0.0.0.4 255.255.255.240 any
access-list 100 permit icmp any host 172.16.253.246 echo
access-list 100 permit tcp any host 172.16.253.247 eq www
ip access-list extended FROM-LOCAL
 permit icmp host 172.16.253.246 any echo-reply
 permit tcp host 172.16.253.247 eq www any established
!
!
!
!
!
!
dial-peer voice 1000 voip
 destination-pattern 1...
 session target ipv4:172.16.240.195
!
dial-peer voice 3000 voip
 destination-pattern 3
 session target ipv4:172.17.2.225
!
telephony-service
 max-ephones 25
 max-dn 25
 ip source-address 172.16.253.241 port 2000
 auto assign 1 to 2
!
ephone-dn 1
 number 2001
!
ephone-dn 2
 number 2002
!
ephone 1
 device-security-mode none
 mac-address 0060.5C42.27CD
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0060.2FD8.4508
 type 7960
 button 1:2
!
ephone 3
 device-security-mode none
 mac-address 0060.5CA3.CC35
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

