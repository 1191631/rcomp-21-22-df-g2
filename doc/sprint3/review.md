RCOMP 2021-2022 Project - Sprint 3 review
=========================================
### Sprint master: 1191590 Hugo Jorge ###

# 1. Sprint's backlog #

In this sprint, each team member will keep working on the same network simulation from the previous
sprint (regarding the same building). From the already established layer three configurations, now OSPF
based dynamic routing will be used to replace static routing used previously.

# 2. Subtasks assessment #

## 2.1. 1191590 - Update the campus.pkt layer three Packet Tracer simulation from the previous sprint for building 1

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 1.

Project folder validations: 

Packet Tracer simulation file for each team member (buildingN.pkt) 
A document (any standard format) detailing DNS databases records on the building’s DNS name server, it may be a screenshot of the server’s DNS database 
Configuration dump for every switch and every router included in the personal folder 

Packet tracer simulation validations: 
OSPF dynamic routing configurations 
DHCP configurations 
DNS server configuration 
VoIP service on the router 
IP phones: 
    -getting the IP address and phone number 
    -can call the other phone 
    -calls forwarding is correct 
Static Firewall (ACLs): 
    -Anti-spoofing and ICMP echo requests 
    -DMZ accesses 
DNAT redirects 

## 2.2. 1200585- Update the campus.pkt layer three Packet Tracer simulation from the previous sprint for building 2

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 2.
Final integration of each member’s Packet Tracer simulation into a
single simulation.

Project folder validations: 

Packet Tracer simulation file for each team member (buildingN.pkt) 
Packet Tracer simulation file for the campus only building B team member (campus.pkt) 
A document (any standard format) detailing DNS databases records on the building’s DNS name server, it may be a screenshot of the server’s DNS database 
Configuration dump for every switch and every router included in the personal folder 

Packet tracer simulation validations: 
OSPF dynamic routing configurations 
DHCP configurations 
DNS server configuration 
VoIP service on the router 
IP phones: 
    -getting the IP address and phone number 
    -can call the other phone 
    -calls forwarding is correct 
Static Firewall (ACLs): 
    -Anti-spoofing and ICMP echo requests 
    -DMZ accesses 
DNAT redirects 

## 2.3. 1191631- Update the campus.pkt layer three Packet Tracer simulation from the previous sprint for building 

Update the campus.pkt layer three Packet Tracer simulation from the
previous sprint, to include the described features in this sprint for
building 3.

Project folder validations: 

Packet Tracer simulation file for each team member (buildingN.pkt) 
A document (any standard format) detailing DNS databases records on the building’s DNS name server, it may be a screenshot of the server’s DNS database 
Configuration dump for every switch and every router included in the personal folder 

Packet tracer simulation validations: 
OSPF dynamic routing configurations 
DHCP configurations 
DNS server configuration 
VoIP service on the router 
IP phones: 
    -getting the IP address and phone number 
    -can call the other phone 
    -calls forwarding is correct 
Static Firewall (ACLs): 
    -Anti-spoofing and ICMP echo requests 
    -DMZ accesses 
DNAT redirects 