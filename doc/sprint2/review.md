RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1200585###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
Create a network simulation matching the developed structured cabling project.
In this sprint the focus is on the layer two infrastructure, and the layer three fundamentals
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:
## 2.1 1191590 -  Development of a a network simulation matching the created structured cabling project for building 1
Project folder validations:

Packet Tracer simulation file for each team member (buildingN.pkt)
Configuration dump for every switch and every router included in the personal folder


Packet tracer simulation validations:

Each cross-connect is represented by a switch
Switches interconnections are in trunk-mode with all VLANs assigned
Correct VLAN database deployed (ALL VLANs defined with correct VLANIDs within provided range)
Representative end-nodes, connected to the correct VLANs (switch ports in access-mode)
All switches are using the correct (provided) VTP domain name
Only the main switch is in VTP server mode, remaining switches are in VTP client mode.
There are redundant layer two links and STP is operating
Switch ports connected to IP phones (7960 model) are well configured

  * All the given tasks were accomplished.
## 2.2 1191631 - Development of a a network simulation matching the created structured cabling project for building 3
Project folder validations:

Packet Tracer simulation file for each team member (buildingN.pkt)
Configuration dump for every switch and every router included in the personal folder


Packet tracer simulation validations:

Each cross-connect is represented by a switch
Switches interconnections are in trunk-mode with all VLANs assigned
Correct VLAN database deployed (ALL VLANs defined with correct VLANIDs within provided range)
Representative end-nodes, connected to the correct VLANs (switch ports in access-mode)
All switches are using the correct (provided) VTP domain name
Only the main switch is in VTP server mode, remaining switches are in VTP client mode.
There are redundant layer two links and STP is operating
Switch ports connected to IP phones (7960 model) are well configured

  * All the given tasks were accomplished.
## 2.3 1200585 - Development of a a network simulation matching the created structured cabling project for building 2
Project folder validations:

Packet Tracer simulation file for each team member (buildingN.pkt)
Configuration dump for every switch and every router included in the personal folder


Packet tracer simulation validations:

Each cross-connect is represented by a switch
Switches interconnections are in trunk-mode with all VLANs assigned
Correct VLAN database deployed (ALL VLANs defined with correct VLANIDs within provided range)
Representative end-nodes, connected to the correct VLANs (switch ports in access-mode)
All switches are using the correct (provided) VTP domain name
Only the main switch is in VTP server mode, remaining switches are in VTP client mode.
There are redundant layer two links and STP is operating
Switch ports connected to IP phones (7960 model) are well configured

  * All the given tasks were accomplished.
## 2.4 1180590  Development of a a network simulation matching the created structured cabling project for building 4

Project folder validations:

Packet Tracer simulation file for each team member (buildingN.pkt)
Configuration dump for every switch and every router included in the personal folder


Packet tracer simulation validations:

Each cross-connect is represented by a switch
Switches interconnections are in trunk-mode with all VLANs assigned
Correct VLAN database deployed (ALL VLANs defined with correct VLANIDs within provided range)
Representative end-nodes, connected to the correct VLANs (switch ports in access-mode)
All switches are using the correct (provided) VTP domain name
Only the main switch is in VTP server mode, remaining switches are in VTP client mode.
There are redundant layer two links and STP is operating
Switch ports connected to IP phones (7960 model) are well configured
  * Building was not added to backbone.
