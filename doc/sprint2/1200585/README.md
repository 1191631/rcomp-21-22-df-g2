RCOMP 2021-2022 Project - Sprint 2 - Member 1200585 folder
===========================================

##Building 2##

## Information
- End user outlets on the ground floor: 25 nodes
- End user outlets on floor one: 50 nodes
- Wi-Fi network: 120 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 12 nodes
- VoIP (IP-phones): 12 nodes


## Distribuction

The range of addresses of my building is 172.16.253.0/21 to 172.17.1.0/21.

| VLAN    | VLAN ID |   Subnet Address   |     Net Mask           |    Available Address Range        | Broadcast Address  |
|---------|---------|--------------------|------------------------|-----------------------------------|--------------------|
| floor0  | 277     |  172.16.253.192    | 255.255.255.224--- /27 |  172.16.253.193 - 172.16.253.222  | 172.16.253.223     |
| floor1  | 278     |  172.16.253.128    | 255.255.255.192---/26  |  172.16.253.129 - 172.16.253.190  | 172.16.253.191     |
| wifi    | 279     |  172.16.253.0      |255.255.255.128-----/25 |  172.16.253.1 -172.16.253.126     | 172.16.253.127     |
| dmz     | 280     |  172.16.253.224    | 255.255.255.240----/28 |  172.16.253.225 - 172.16.253.238  | 172.16.253.239     |
| voip    | 281     |  172.16.253.240    | 255.255.255.240----/28 |  172.16.253.241-172.16.253.254    | 172.16.253.255     |


## Notes

* The switch called "2-IC-Floor0" is the one assuming the server mode within the isolated building 2 isolated simulation.
* VTP domain rcompdfg2.
* IPv4 node address each router will use in the backbone network is ip address 172.16.251.4