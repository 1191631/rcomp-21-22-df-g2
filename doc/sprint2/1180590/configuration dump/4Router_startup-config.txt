!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017798Q-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.1
 encapsulation dot1Q 290
 ip address 172.17.6.1 255.255.255.224
!
interface FastEthernet0/0.2
 encapsulation dot1Q 291
 ip address 172.17.6.32 255.255.255.192
!
interface FastEthernet0/0.3
 encapsulation dot1Q 292
 ip address 172.17.6.89 255.255.255.128
!
interface FastEthernet0/0.4
 encapsulation dot1Q 293
 ip address 172.17.6.162 255.255.255.240
!
interface FastEthernet0/0.5
 encapsulation dot1Q 294
 ip address 172.17.6.175 255.255.255.240
!
interface FastEthernet0/0.6
 encapsulation dot1Q 275
 ip address 172.16.251.5 255.255.255.128
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router rip
!
ip classless
ip route 172.16.249.0 255.255.255.0 172.16.251.3 
ip route 172.16.253.0 255.255.255.0 172.16.251.4 
ip route 0.0.0.0 0.0.0.0 172.16.251.3 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

