RCOMP 2021-2022 Project - Sprint 2 - Member 1180590 folder
===========================================

##Building 4##

## Information

- End user outlets on the ground floor: 28 nodes
- End user outlets on floor one: 55 nodes
- Wi-Fi network: 70 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 10 nodes
- VoIP (IP-phones): 12 nodes

## Distribuition

The range of addresses to my building is 172.17.6.0/21 to 172.17.9.0/21.

| VLAN     | VLAN ID |   Subnet Address   |       Net Mask        |    Available Address Range        | Broadcast Address  |
|----------|---------|--------------------|-----------------------|-----------------------------------|--------------------|
| Floor 0  | 290     | 172.17.6.0         | /27 - 255.255.255.224 |   172.17.6.1    -   172.17.6.29   | 172.17.6.30        |
| Floor 1  | 291     | 172.17.6.31        | /26 - 255.255.255.192 |   172.17.6.32   -   172.17.6.87   | 172.17.6.88        |
| Wi-Fi    | 292     | 172.17.6.89        | /25 - 255.255.255.128 |   172.17.6.89   -   172.17.6.159  | 172.17.6.160       |
| DMZ      | 293     | 172.17.6.161       | /28 - 255.255.255.240 |   172.17.6.162  -   172.17.6.172  | 172.17.6.173       |
| VoIP     | 294     | 172.17.6.174       | /28 - 255.255.255.240 |   172.17.6.175  -   172.17.6.187  | 172.17.6.188       |
