RCOMP 2021-2022 Project - Sprint 2 - Member 1191631 folder
===========================================
(This folder is to be created/edited by the team member 1191631 only)

##Building 3##

## Information

- End user outlets on the ground floor: 35 nodes
- End user outlets on floor one: 45 nodes
- Wi-Fi network: 55 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 28 nodes
- VoIP (IP-phones): 25 nodes

## Distribuction

The range of addresses to my building is 172.17.2.0/21 to 172.17.5.0/21.

| VLAN            | VLAN ID |   Subnet Address   |       Net Mask        |    Available Address Range        | Broadcast Address  |
|-----------------|---------|--------------------|-----------------------|-----------------------------------|--------------------|
| 3_Floor0        | 284     | 172.17.2.0         | /26 - 255.255.255.192 |    172.17.2.1 - 172.17.2.62       | 172.17.2.63        |
| 3_Floor1        | 285     | 172.17.2.64        | /26 - 255.255.255.192 |    172.17.2.65 - 172.17.2.126     | 172.17.2.127       |
| 3_Wi-Fi         | 286     | 172.17.2.128       | /26 - 255.255.255.192 |    172.17.2.129 - 172.17.2.190    | 172.17.2.191       |
| 3_DMZ           | 287     | 172.17.2.192       | /27 - 255.255.255.224 |    172.17.2.193 - 172.17.2.222    | 172.17.2.223       |
| 3_VoIP          | 288     | 172.17.2.224       | /27 - 255.255.255.224 |    172.17.2.225 - 172.17.2.254    | 172.17.2.255       |


## Notes

* The switch called "3-IC-Floor_0" is the one assuming the server role.
* VTP domain rcompdfg2.
* IPv4 node address each router will use in the backbone network is ip address 172.16.251.5