RCOMP 2021-2022 Project - Sprint 2 planning
===========================================
### Sprint master: 1200585 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
Create a network simulation matching the developed structured cabling project. 
In this sprint the focus is on the layer two infrastructure, and the layer three fundamentals

# 2. Technical decisions and coordination #
Since all simulations created must all be put together in a single simulation, different Packet Tracer versions might raise some issues and, for that reason, every team member will be using the same version. In our case, it's going to be version 8.1.1

Each VLAN should have a unique meaningful name and a unique VLANID. In order to avoid any interference,
each team is required to use a specific range of values for VLANID's, and according to the "Per team specific data -
mandatory use by teams" chapter in the sprint 2 description and backlog, our team was given the values from 270 up to 300.
To be more specific with the values' distribution,

-Member of the team 1191590 is going to use values from 270 to 276;

-Member of the team 1200585 is going to use values from 277 to 283;

-Member of the team 1191631 is going to use values from 284 to 290;

-Member of the team 1180590 is going to use values from 291 to 297.

The default VLAN, with 1 as the ID, is going to be used to connect to the backbone.

When all simulations are put together, there will be only one switch with the VTP server role, so the VTP domain must be the same on every switch.
According to the chapter mentioned earlier, our team was assigned with ***rcompdfg2*** as the domain name to be used.

The IPv4 network address of the backbone network, this network address must belong to the address space provided to the team (addresses block),
and yet it can’t overlap addresses blocks assigned to team members for use within each building. For that reason, our team was assigned with values ranging from 172.16.249.0/21 up
to 172.17.9.0/21 In order to avoid overlapping,

-Member of the team 1191590 is going to use values from 172.16.249.0/21 to 172.16.252.0/21;

-Member of the team 1200585 is going to use values from 172.16.253.0/21 to 172.17.1.0/21;

-Member of the team 1191631 is going to use values from 172.17.2.0/21 to 172.17.5.0/21;

-Member of the team 1180590 is going to use values from 172.17.6.0/21 to 172.17.9.0/21;

At last, our team was assigned with 15.203.47.117/30 as the ISP router IPv4 node address.

All devices will be named using the following formula : "XX"-"YY"-"ZZ"
           -"XX" represents the number of the building
           -"YY" represents the type of device being used, ex: CP(Consolidation Point), HC(Horizontal Cross-Connect) 
           -"ZZ" represents the floor in which the device is located

The IPv4 network address assigned to the backbone network is the following:

- 172.16.251.2



The IPv4 node address each router will use in the backbone network is the following:

-Member of the team 1191590 is going to use the ip address 172.16.251.3;

-Member of the team 1200585 is going to use the ip address  172.16.251.4;

-Member of the team 1191631 is going to use the ip address 172.16.251.5;



# 3. Subtasks assignment #
1191590- (T.2.1) Development of a layer two and layer three Packet Tracer simulation for building 1, and also encompassing the campus backbone.
Integration of every members’ Packet Tracer simulations into a single simulation.

1200585 - (T.2.2) Development of a layer two and layer three Packet Tracer simulation for building 2, and also encompassing the campus backbone.

1191631 - (T.2.3) Development of a layer two and layer three Packet Tracer simulation for building 3, and also encompassing the campus backbone.

1180590 - (T.2.4) Development of a layer two and layer three Packet Tracer simulation for building 4, and also encompassing the campus backbone.

