RCOMP 2021-2022 Project - Sprint 2 - Member 1191590 folder
===========================================

##Building 1 & Backbone##

## Information

- End user outlets on the ground floor: 60 nodes
- End user outlets on floor one: 80 nodes
- Wi-Fi network: 120 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 100 nodes
- VoIP (IP-phones): 40 nodes
- Backbone: 120 nodes

## Distribuition

The range of addresses to this building and the backbone is 172.16.249.0/21 to 172.16.252.0/21.

| VLAN     | VLAN ID |   Subnet Address   |       Net Mask        |    Available Address Range        | Broadcast Address  |
|----------|---------|--------------------|-----------------------|-----------------------------------|--------------------|
| Floor 0  | 270     | 172.16.249.0       | /26 - 255.255.255.192 |   172.16.249.1 - 172.16.249.62    | 172.16.2.63        |
| Floor 1  | 271     | 172.16.249.64      | /25 - 255.255.255.128 |  172.16.249.65 - 172.16.249.192   | 172.16.249.193     |
| Wi-Fi    | 272     | 172.16.249.194     | /25 - 255.255.255.128 |  172.16.249.195 - 172.16.250.64   | 172.16.250.65      |
| DMZ      | 273     | 172.16.250.66      | /25 - 255.255.255.128 |   172.16.250.67 - 172.16.250.192  | 172.16.250.193     |
| VoIP     | 274     | 172.16.250.194     | /26 - 255.255.255.192 |  172.16.250.195 - 172.16.251.0    | 172.16.251.1       |
| Backbone | 275     | 172.16.251.2       | /25 - 255.255.255.128 |   172.16.251.3 - 172.16.251.128   | 172.16.251.129     |


Nº Nodes
60	   26/64/0.0.0.64
80	   25/128/0.0.0.128	
120	   25/128/0.0.0.128	
100	   25/128/0.0.0.128	
40	   26/64/0.0.0.64
120	   25/128/0.0.0.128	

Total: 520