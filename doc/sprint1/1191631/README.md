
RCOMP 2021-2022 Project - Sprint 1 - Member 1191631 folder
===========================================

# Building 3

## Floor 0
![Floor0](Floor0.png)

## Area Calculation and Outlets needed:
>-For every 10m² plus 2 Outlets.

| Room  | Length(m)      | Width(m)    |  Area m²    | Outlets            |
|-------|----------------|-------------|-------------|--------------------|
| 3.0.1 | 3,06m          |  5,25m      |  16,065m²   | 4                  |
| 3.0.2 | 3,06m          |  5,25m      |  16,065m²   | 4                  |
| 3.0.3 | 3,06m          |  5,25m      |  16,065m²   | 4                  |
| 3.0.4 | 3,61m          |  5,25m      |  18,9525m²  | 4                  |
| 3.0.5 | 8,61m          |  3,32m      |  28,5852m²  | 6 + 1 ACCESS POINT |
| 3.0.6 | 8,61m          |  3,32m      |  28,5852m²  | 6                  |
| 3.0.7 | 8,61m          |  3,32m      |  28,5852m²  | 6                  |
| 3.0.8 | 3,33m          |  4,15m      |  13,8195m²  | 4                  |
| 3.0.9 | 3,33m          |  2,49m      |  8,2917m²   | no network outlets |
Total 39 Outlets  

## Cable Calculations:

###Room 3.0.1

1- 8,5+5 = 13,5m \
2- 8,5+1,5 = 10m \
3- 5,5+5 = 10,5m \
4- 5,5+1,5 = 7m 
####Total : 41m


###Room 3.0.2

1- 5,10+5 = 10,5m \
2- 5,10+1,5 = 6,6m \
3- 2,20+5 = 7,2m \
4- 2,20+1,5 = 3,7m 
####Total : 28m


###Room 3.0.3

1- 1,10+5 = 6,1m \
2- 1,10+1,5 = 2,6m \
3- 1,80+5 = 6,8m \
4- 1,80+1,5 = 3,3m 
####Total : 18,8m

##### CP- 4+12,5+16 = 32,5m
For Rooms 3.0.1 | 3.0.2 | 3.0.3 | 3.0.4 --> 16 Ports

###Room 3.0.4

1- 4+2,50+5 = 11,5m \
2- 4+2,50+1,5 = 8m \
3- 5+5 = 10m \
4- 5+1,5 = 6,5m 
####Total : 36m


##Room 3.0.5

1- 4+6 = 10m \
2- 4+3 = 7m \
3- 4+0,2 = 4,2m \
4- 4+1+3,2+6 = 14,2m \
5- 4+1+3,2+3 = 11,2m \
6- 4+1+3,2+1 = 9,2m 
#####AP- 4+7,5+1+2,5 = 15m 
####Total :  70,8m


##Room 3.0.6

1- 6 = 6m \
2- 3 = 3m \
3- 0,2 = 0,2m \
4- 1+3,2+6 = 10,2m \
5- 1+3,2+3 = 7,2m \
6- 1+3,2+1 = 5,2m 
####Total : 31,8m

####CP- 16m 
For Rooms 3.0.5 | 3.0.6 | 3.0.7 --> 19 Ports

##Room 3.0.7

1- 4+6 = 10m \
2- 4+3 = 7m \
3- 4+0,2 = 4,2m \
4- 4+1+3,2+6 = 14,2m \
5- 4+1+3,2+3 = 11,2m \
6- 4+1+3,2+1 = 9,2m 
####Total :  55,8m


##Room 3.0.8

1- 2,5+7+3,30+1,5 = 14,3m \
2- 2,5+7+1,5 = 11m \
3- 2,5+4,5 = 7m \
4- 2,5+3+1,6 = 7,1m 
####Total : 39,4m

For Room 3.0.8 use the HC



###AllRooms 321,6m 
###Rooms + CPs = 370,1m

	

##Floor Inventory:

####2 CP
	1 CP
      16 Copper Patch Cords for Outlets
      16 Copper Patch Cords for Patch Panels
	1 CP 
	  19 Copper Patch Cords for Outlets
	  19 Copper Patch Cords for Patch Panels
	2 copper patch panels patch panel of 24 ports
	2 switches of 24 ports 
	2 Telecommunications Enclosures - Units in use 2U -> Units needed 6U

####1 HC
	4 Copper Patch Cords for Outlets
	4 Copper Patch Cords for Patch Panels
	1 copper patch panels patch panel of 24 ports 
	1 switches de 24 ports 
	1 Telecommunications Enclosures - Units in use 2U -> Units needed 6U

####1 IC 
	10 Fiber Patch Cords for Patch Panels
	1 fibre patch panel of 24 ports 
 	1 switches of 24 ports 
	1 Telecommunications Enclosures - Units in use 2U -> Units needed 6U

###Total
39 Outlets \
39 Copper Patch Cords for Outlets *5 --> 195m \
39 Copper Patch Cords for Patch Panels *0,5 --> 19,5m

####Total Copper Cable : 370,1+195+19,5= 584,6m

10 Fiber Patch Cords for Patch Panels *0,5 -- 5m

#####Total Optic Fiber Cable : 18,6+12,90+1,5+2 = 35m || With Redundancy : 70m || 70+5 = 75m
>Optical fiber measurement from underfloor cable raceway connected to the external technical ditch.



>	The use of two Consolidation Points serves to reduce the length of cable expended for all outlets if
had to be connected to Cross-Connect.\
    For a full wireless LAN (Wi-Fi) coverage is for this floor is only need one access point, 
because access point device will grant a 50 meters diameter circle coverage what is less than the building area.\
	Telecommunications Enclosures Formula: 6xS for Telecommunications Enclosures measurements.\
Patch cords for users have a length of 5 m. and the patch cords for patch panels have a length of 0,5m.

## Floor 1
![Floor1](Floor1.png)

## Area Calculation and Outlets needed:
>-For every 10m²  plus 2 Outlets.

| Room    | Length(m)      | Width(m)    |  Area m²    | Outlets            |
|---------|----------------|-------------|-------------|--------------------|
| 3.1.1   | 6,11m          | 4,70m       |  28,717m²   | 6                  |             
| 3.1.2   | 6,39m          | 4,70m       |  30,033m²   | 8                  |            
| 3.1.3   | 4,17m          | 4,98m       |  20,7666m²  | 6                  |           
| 3.1.4   | 4,17m          | 4,70m       |  19,599m²   | 4                  |          
| 3.1.5   | 4,17m          | 4,70m       |  19,599m²   | 4 + 1 ACCESS POINT |
| 3.1.6   | 4,17m          | 4,70m       |  19,599m²   | 4                  |
| 3.1.7   | 6,39m          | 4,15m       |  26,5185m²  | 6                  |
| 3.1.8   | 6,39m          | 4,15m       |  26,5185m²  | 6                  |
| 3.1.9   | 6,11m          | 4,15m       |  25,3565m²  | 6                  |
|  3.1.10 | 3,33m          | 2,49m       |  8,2917m²   | no network outlets |
Total                                                     51     

## Cable Calculations:

###Room 3.1.1

1- 9+3,75+4 = 16,75m \
2- 9+1+4 = 14m \
3- 6+4 = 10m \
4- 9+4,7+3+4 = 20,7m \
5- 9+4,7+6+1+4 = 24,7m \
6- 9+4,7+6+3+4 = 26,7m 
####Total : 112,85m


###Room 3.1.2

1- 1,5+0,75 = 2,25m \
2- 1,5+3 = 4,5m \
3- 1,5+3,75+1,8 = 7,05m \
4- 1,5+3,75+3,7 = 8,95m \
5- 1,5+3,75+4,7+1,75 = 11,7m \
6- 1,5+3,75+4,7+4,5 = 14,45m \
7- 1,5+3,75+4,7+6,25+1 = 17,2m \
8- 1,5+3,75+4,7+6,25+3 = 19,2m 
####Total : 85,8m

####CP- 14,6+7,7+4= 26,3m
For Rooms 3.1.1 | 3.1.2 | 3.1.3 --> 20 Ports

###Room 3.1.3

1- 0,4+0,3+2+4 = 6,7m\
2- 0,4+0,3+4,2+1,2+4 = 10,1m\
3- 0,4+0,3+4,2+3,5+4 = 12,4m\
4- 0,4+0,3+4,2+4,9+2+4 = 15,8m\
5- 0,4+0,3+4,2+4,9+4+0,8+4 = 18,6m\
6- 0,4+0,3+4,2+4,9+4+2,5+4 = 20,3m
####Total : 83,9m


###Room 3.1.4

1- 5,3+1,6+4 = 10.9m\
2- 5,3+4+4 = 13,3m\
3- 5,3+4,2+1+4 = 14,5m\
4- 5,3+4,2+3,4+4 = 16,9m
####Total : 55,6m


##Room 3.1.5

1- 0,4+1,6+4 = 6m\
2- 0,4+4+4 = 8,4m\
3- 0,4+5+4,2+1+4 = 14,6m\
4- 0,4+5+4,2+3,4+4 = 17m
#####AP- 0,4+2+2,9 = 5,3m

####Total : 51,3m


##Room 3.1.6

1- 1,5+0,7 = 2,2m\
2- 1,5+2,2+4,2+0,8 = 8,7m\
3- 1,5+2,2+4,2+3 = 10,9m\
4- 1,5+2,2+4,2+4,7+2 = 14,6m
####Total : 36,4m

#####CP- 14,2+4 = 18,2m
For Rooms 3.1.5 | 3.1.6 | 3.1.7 --> 15 Ports

##Room 3.1.7

1- 2,7+1+4 = 7,7m\
2- 2,7+4,2+1+4 = 11,9m\
3- 2,7+4,2+3,5+4 = 14,4m\
4- 2,7+4,2+4,1+2,5+4 = 17,5m\
5- 2,7+4,2+4,1+4,9+4 = 19,9m\
6- 2,7+4,2+4,1+6,3+2,2+4 = 23,5m
####Total : 94,9m


##Room 3.1.8

1- 2,7+8,8+4 = 15,5m \
2- 2,7+11,7+1+4 = 19,4m\
3- 2,7+11,7+3,4+4 = 21,8m\
4- 2,7+11,7+4+2,5+4 = 24,9m\
5- 2,7+11,7+4+4,5+4 = 26,9m\
6- 2,7+11,7+4+6,3+2,2+4 = 30,9m
####Total : 139,4m


##Room 3.1.9

1- 2,7+1+1+4 = 8,7m\
2- 2,7+1+3,4+4 = 11,1m\
3- 2,7+1+4,2+2+4 = 13,9m\
4- 2,7+1+4,2+4,8+4 = 16,7m\
5- 2,7+1+4,2+6,2+2,3+4 = 20,4m\
6- 2,7+1,6+4 = 8,3m
####Total : 79,1m

###All Rooms 739,25
###Rooms+CPs = 783,75m


##Floor Inventory:


### 2 CP
	1 CP
      20 Copper Patch Cords for Outlets
      20 Copper Patch Cords for Patch Panels
    1 CP
	  15 Copper Patch Cords for Outlets
      15 Copper Patch Cords for Patch Panels
	2 copper patch panel of 24 portas  
	2 switches of 24 portas
	2 Telecommunications Enclosures - Units in use 2U -> Units nedded 6U

### 1 HC
	16 Copper Patch Cords for Outlets
	16 Copper Patch Cords for Patch Panels
	1 copper patch panel of 24 portas  Units in use 2U -> Units nedded 6U
	1 switches of 24 portas
	1 Telecommunications Enclosures - Units in use 2U -> Units nedded 6U

###Total

51 Outlets\
51 Copper Patch Cords for Outlets * 5 --> 255m\
51 Copper Patch Cords for Patch Panels * 0,5 --> 25,5m

####Total Copper Cable 255+25,5+789,75= 1071m

####Total Optic Fiber Cable 1,35+0,2+4,5+1,5+0.2 = 7,75m || With Redundancy 15,5m

>	The use of two Consolidation Points serves to reduce the length of cable expended if all outlets had to be connected to Cross-Connect.\
  The space over the dropped ceiling is to install cable raceways and wireless access-point.\
> For a full wireless LAN (Wi-Fi) coverage is for this floor is only need one access point (wireless 802 11 device), 
because access point device will grant a 50 meters diameter circle coverage what is less than the building area, this device is
slightly not aligned with the access point from the floor 0.\
>Telecommunications Enclosures Formula: 6xS for Telecommunications Enclosures measurements.\
> Patch cords for users have a length of 5 m. and the patch cords for patch panels have a length of 0,5m.
