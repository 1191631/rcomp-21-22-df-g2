RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1191631 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
The project consists in private closed area with 5 buildings (named from 1 to 5) that each have two floors. Because the working group consists of 3 people
and one element works only with one building, we will work from building 1 to 3.
A floor plant from each building is given as well as the plant for the building itself. Represented in blue are the passage ways for the cabling that is going to be installed in this compound and in each building.  
Each building has it's own specifications and requirements. The decisions about these requirements will be explained below.

# 2. Technical decisions and coordination #
* Building 1:
  This building holds the datacentre, it will also house the main cross-connect for the  structured cabling system. Both floors must have wireless LAN coverage (Wi-Fi).
    Floor 0:The ground floor has an underfloor cable raceway connected to the external technical ditch. Access to
            the underfloor cable raceway is available at points marked over the plan. Also, cable passageways to the
            above floor are available
            The ceiling height on this floor is 4 meters. Common areas, like the entrance hall, restrooms, and stairs,
            require no network outlets.
    Floor 1:The ceiling height on this floor is 3 meters, however there’s a removable dropped ceiling, placed 2.5
            meters from the ground, covering the entire floor. The space over the dropped ceiling is perfect to install
            cable raceways and wireless access-points, this floor has no underfloor cable raceways.
            Room 1.1.1 is a storage area and may be used to house a cross-connect, no network outlets are required
            there, and the same applies to restrooms and common areas like corridors and halls. Elsewhere, in every
            identified room, the standard number of network outlets per area rate should be honoured
* Building 2:
   Both floors require full wireless LAN coverage (Wi-Fi).
    Floor 0:The ground floor has an underfloor cable raceway connected to the external technical ditch. Access to
            the underfloor cable raceway is available at points marked over the plan. The ceiling height on this floor
            is 4 meters.
            Room 2.0.1 is a storage area and may be used to house a cross-connect, no network outlets are required
            there, and the same applies to the restrooms, the entrance hall, and other common areas.
    Floor 1:This floor has no underfloor cable raceway. The ceiling height on this floor is 3 meters, however there’s
            a removable dropped ceiling, placed 2.5 meters from the ground, covering the entire floor. The space
            over the dropped ceiling is perfect to install cable raceways.
            Room 2.1.1 is a storage area and may be used to house a cross-connect, no network outlets are required
            there, and the same applies to restrooms and corridors.
* Building 3:
   Both floors require full wireless LAN coverage (Wi-Fi).
    Floor 0:The ground floor has an underfloor cable raceway connected to the external technical ditch. Access to
            the underfloor cable raceway is available at points marked over the plan. The ceiling height on this floor
            is 4 meters.
            Room 3.0.9 is a storage area that may be used to house a cross-connect, no network outlets are required
            there, and the same applies to restrooms and the entrance hall.
    Floor 1:The ceiling height on this floor is 3 meters, but there’s a removable dropped ceiling, placed 2.5 meters
           from the ground, covering this entire floor. The space over the dropped ceiling is perfect to install cable
           raceways and wireless access-points.
           Common areas and restrooms are not required to have network outlets, rooms should be provided with
           the standard number of network outlets. Room 3.1.10 is a storage area, no network outlets are required
           there as well, and it may be used to house a cross-connect and other network infrastructure hardware

We will be using optical fiber cable for the connections between buildings, IC's and HC's for optimal efficiency. 
The copper cable (Cat7) will be used in connections from the HC's to the CP's or outlets.

All the optical fiber is monomode.

The RJ45 connectors and Patch Panels will be used according to the standard color scheme 568A.

The buildings are connected between them using redundancy so that, in a case of a failure, they always have a physical
connection between them and this minimizes the impact of such a failure, dual cables will be installed so that if one fails,
others continue to operate. This also has the advantage of allowing more traffic at heights
where the network has high affluence.

We used the technical ditch to distribute the network to all buildings using fiber optic cables. The use of fiber optics
is related to the fact that it is able to cover large distances without the need for repeaters
and maintaining a high transmission rate.



# 3. Subtasks assignment #
  
  * 1191590 - Development of a structured cabling project for building 1,and also encompassing the campus backbone
  * 1191631 - Development of a structured cabling project for building 3,and also encompassing the campus backbone
  * 1200585 - Development of a structured cabling project for building 2,and also encompassing the campus backbone

