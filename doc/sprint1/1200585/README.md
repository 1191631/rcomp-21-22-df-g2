RCOMP 2021-2022 Project - Sprint 1 - Member 1200585 folder
===========================================




## Building 2

Subtask: Development of a structured cabling project for building 2.

Scale: 2,9cm <-> 5m

|   Room    | C (m)  | L (m)  |     A (m²)       | Outlets |
|-----------|---------|---------|------------------|---------|             
| 2.0.1      | 3.4         | 2.9      | 9.8        | 0                    
| 2.0.2      | 5           | 5.7      | 28.5       | 6                     
| 2.0.3     | 8.4         | 10       | 84         | 18                      
| 2.0.4     | 8.4         | 3.3      | 27.7   	  | 6   
| 2.0.5     | 8.4         | 3.3      | 27.7   	  | 6
| 2.0.6     | 8.4         | 3.3      | 27.7   	  | 6                   
| 2.1.1     | 3.4         | 2.9      | 6.46   	  | 0                                            
| 2.1.2     | 5.1         | 2.9      | 14.8   	  | 4  
| 2.1.3     | 5.1         | 2.9      | 14.8  	  | 4  
| 2.1.4     | 5.1         | 2.6      | 13.3   	  | 4  
| 2.1.5     | 5.1         | 2.6      | 13.3   	  | 4  
| 2.1.6     | 5.1         | 3.1      | 15.8   	  | 4
| 2.1.7     | 6.2         | 3.3      | 20.5   	  | 6  
| 2.1.8     | 6.2         | 3.3      | 20.5   	  | 6  
| 2.1.9     | 5           | 2.9      | 14.5   	  | 4
| 2.1.10    | 5           | 2.9      | 14.5   	  | 4
| 2.1.11    | 5           | 2.9      | 14.5   	  | 4  
| 2.1.12    | 5           | 3.4      | 17   	  | 4


### Floor Zero (Ground Floor)

This floor has:
- 1 Intermediate cross-connect (IC)
   - 1 fiber patch panel with 24 ports
   - 1 switch with 24 ports each
- 1 Horizontal cross-connect (HC)
   - 1 copper CAT-7 patch panel with 24 ports
   - 1 CAT-7 switch with 24 ports each
- 2 Consolidation Points (CP)
    - 2 copper CAT-7 patch panels with 24 ports
    - 2 CAT-7 switches with 24 ports each
- 1 Access Points (Ac)
- 42 Outlets 
- 42 Copper Patch Cords for Outlets (*5 -- 210m)
- 42 Copper Patch Cords for Patch Panels (*0,5 -- 21m)
- 10 Fiber Patch Cords for Patch Panels (*0,5 -- 5m)
- 3,08m of Optical Fibre cable, 6m with redundancy 
- 426,4m of Copper Cables
- 4 Telecommunications Enclosures (6U)

![Buiding 2- Floor Zero](building2piso0.jpg)

### Floor One

This floor has:
- 1 Horizontal cross-connect (HC)
  - 1 copper CAT-7 patch panel with 24 ports
  - 1 switch with 24 ports each
- 3 Consolidation Points (CP)
  - 3 copper CAT-7 patch panels with 24 ports each
  - 3 CAT-7 switches with 24 ports each
- 1 Access Points (Ac)
- 48 Outlets
- 48 Copper Patch Cords for Outlets *5 --> 240m
- 48 Copper Patch Cords for Patch Panels *0,5 --> 24m
- 2m of Optical Fibre cable, 4m with redundancy
- 380,8m of Copper Cables
- 4 Telecommunications Enclosures (6U)

![Buiding 2- Floor One](building2piso1.jpg)




    