RCOMP 2021-2022 Project - Sprint 1 review
=========================================
### Sprint master: 1191631 ###

# 1. Sprint's backlog #
The project consists in embracing a private building compound with 5 buildings (named from 1 to 5) that each have two floors. Because the working group consists of 3 people
and one element works only with one building, we will work from building 1 to 3.
A floor plant from each building is given as well as the plant for the building itself. Represented in blue are the passage ways for the cabling that is going to be installed in this compound and in each building.  
Each building has it's own specifications and requirements.

# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:
* 2.1 1191590 - Development of a structured cabling project for building 1,and also encompassing the campus backbone
    * All the given tasks were accomplished.
* 2.2 1191631 - Development of a structured cabling project for building 3,and also encompassing the campus backbone
    * All the given tasks were accomplished.
* 2.3 1200585 - Development of a structured cabling project for building 2,and also encompassing the campus backbone
    * All the given tasks were accomplished.
    * A better solution for the cable pathways in the floor 1 could be used.






