RCOMP 2021-2022 Project - Sprint 1 - Member 1191590 folder
===========================================
# Building 1 - Floor 0

|   Room    | C (m)  | L (m)  |     A (m²)       | Outlets |
|-----------|---------|---------|------------------|---------|
|   1.0.1   | 6,6     | 3,1     | 20,46            | 4      |
|   1.0.2   | 6,6     | 4,4     | 29,04            | 0      |
|   1.0.3   | 6,6     | 4,4     | 29,04            | 6      |
|   1.0.4   | 6,6     | 6,7     | 44,22            | 8       |
|   1.0.5   | 5,0     | 3,0     | 15,00            | 3      |
|   1.0.6   | 5,0     | 3,0     | 15,00           | 3      |
|   1.0.7   | 5,0     | 3,0     | 15,00           | 3      |
|   1.0.8   | 5,0     | 5,0     | 25,00           | 5      |
|   1.0.9   | 5,8     | 4,7     | 27,26           | 6      |
|   1.0.10   | 5,8     | 7,2     | 41,76           | 8      |

> Note: Room 1.0.2 does not have any outlet as the equipment to be placed there will not need them

## Floor 0 Diagram
![floor0](floor0.png)

#### Floor 0 Inventory
* 46 outlets
* 8 meters monomode optic fiber
* 442 metros de CAT-7
* 1 access point (wireless 802.11)
* 1 IC
* 1 HC:
  * 19 patch cords CAT-7
  * 1 switch CAT-7 24 ports
  * 1 patch panel CAT-7 24 ports
* 2 CPs:
  * 30 patch cords
  * 2 switch CAT-7 24 ports
  * 2 patch panel CAT-7 24 ports
* 3 Telecommunication Enclosure (U6)

# Building 1 - Floor 1
|   Room   | C (m)  | L (m)  |     A (m²)       | Outlets |
|----------|---------|---------|------------------|---------|
|   1.1.1   | 6,6     | 3,0     | 19,80           | 0      |
|   1.1.2   | 6,6     | 3,0     | 19,80           | 4      |
|   1.1.3   | 6,6     | 3,0     | 19,80           | 4      |
|   1.1.4   | 5,0     | 2,8     | 14,00            | 3      |
|   1.1.5   | 5,0     | 2,8     | 14,00            | 3      |
|   1.1.6   | 5,0     | 2,8     | 14,00            | 3      |
|   1.1.7   | 5,0     | 2,8     | 14,00            | 3      |
|   1.1.8   | 5,0     | 2,8     | 14,00            | 3      |
|   1.1.9   | 5,0     | 2,8     | 14,00            | 3      |
|   1.1.10   | 5,0     | 2,8     | 14,00           | 3      |
|   1.1.11   | 5,0     | 2,8     | 14,00           | 3      |
|   1.1.12   | 5,0     | 2,8     | 14,00           | 3      |
|   1.1.13   | 5,0     | 2,8     | 14,00           | 3      |
|   1.1.14   | 5,8     | 7,2     | 41,76           | 8      |

> Note: Room 1.1.1 does not have any outlet as it will be a storage room

Total Outlets= 92

## Floor 1 Diagram
![floor1](floor1.png)

#### Floor 1 Inventory
* 46 outlets
* 5 meters monomode optic fiber
* 411 meters CAT-7
* 1 access point (wireless 802.11)
* 1 IC
* 1 HC:
  * 9 patch cord
  * 1 switch CAT-7 24 ports
  * 1 patch panel CAT-7 24 ports
* 2 CPs:
  * 40 patch cords
  * 1 switch CAT-7 48 ports
  * 1 patch panel CAT-7 48 ports
  * 1 switch CAT-7 24 ports
  * 1 patch panel CAT-7 24 ports
* 3 Telecommunication Enclosure (U6)
