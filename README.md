RCOMP 2021-2022 Project repository template
===========================================
# 1. Team members #
  * 1191631 - {Ricardo Moreira} 
  * 1191590 - {Hugo Jorge} 
  * 1200585 - {Manuel Silva} 
  * 1180590 - {João Teixeira}

# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)

